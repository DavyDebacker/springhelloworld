package helloworld;

import static org.junit.Assert.*;
import helloworld.data.MessageHibernateDAO;
import helloworld.data.MessageRepository;
import helloworld.model.Message;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class DavysTest {
	
	private static final String HELLO_FROM_YET_ANOTHER_SIDE = "Hello from yet another side";
	
	private static final String HELLO_FROM_THE_OTHER_SIDE = "Hello from the other side";
	
	private AnnotationConfigApplicationContext context;
	
	private MessageRepository messageRepository;
	
	@Before
	public void setUp() {
		context = new AnnotationConfigApplicationContext(TestConfig.class, MessageHibernateDAO.class);
		
		messageRepository = context.getBean(MessageRepository.class);
		
		assertNotNull(messageRepository);
	}
	
	@Test
	public void dummyTest() {
		assertTrue(messageRepository.getAll().isEmpty());
		
		Message testMessage = new Message("DavyDebacker", HELLO_FROM_THE_OTHER_SIDE);
		messageRepository.save(testMessage);
		assertFalse(messageRepository.getAll().isEmpty());
		
		List<Message> actualMessages = messageRepository.getMessagesBySender("DavyDebacker");
		Message actualMessage = actualMessages.get(0);
		assertEquals(HELLO_FROM_THE_OTHER_SIDE, actualMessage.getMessage());
		
		actualMessage.setMessage(HELLO_FROM_YET_ANOTHER_SIDE);
		messageRepository.update(actualMessage);
		
		List<Message> actualMessages2 = messageRepository.getMessagesBySender("DavyDebacker");
		Message actualMessage2 = actualMessages2.get(0);
		assertEquals(HELLO_FROM_YET_ANOTHER_SIDE, actualMessage2.getMessage());
	}
	
	@After
	public void cleanUp() {
		context.close();
	}
	
}
