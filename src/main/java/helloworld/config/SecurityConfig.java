package helloworld.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.
                            configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.
                                    configuration.EnableWebMvcSecurity;
@Configuration
//MIND YOU: not just @EnableWebSecurity
@EnableWebMvcSecurity
 public class SecurityConfig extends WebSecurityConfigurerAdapter {

		//let's create our authentication system, here it is in memory
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication()
			.withUser("admin").password("admin").roles("ADMIN","USER").and()
			.withUser("pieterjd").password("pieterjd").roles("ADMIN");
	}

	//let's create the authorization part here, some urls are public, others need authentication
	//only authentication for the add message part, others are public
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		//super.configure(http);
		//by overriding this method, you need to add the formLogin explictly
		http
		
		
		.authorizeRequests()
		.antMatchers(HttpMethod.GET,"/addMessage").hasRole("ADMIN")
		
		.anyRequest().permitAll()
		.and()
		.formLogin().loginPage("/login")
		.and()
		.csrf().disable() // do not turn this off in reallife
		;
		
	}
	
}