package helloworld.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

import helloworld.data.MessageHibernateDAO;
import helloworld.data.MessageRepository;





@Configuration
public class DataConfig {
	/*
	@Bean
	public MessageRepository messageRepository(SessionFactory factory){
		return new MessageHibernateDAO(factory);
	}
	*/
	
	
	//the datasource bean
	@Bean
	public BasicDataSource dataSource(){
		BasicDataSource ds = new BasicDataSource();
		ds.setUrl( "jdbc:mysql://localhost:3306/pieterjd");
		ds.setUsername("pieterjd");
		ds.setPassword("19ABcd78");
		ds.setDriverClassName("com.mysql.jdbc.Driver");
		
		//ideally, create pool of 5 to 10 connections to database - is necessary when working with db4free.net
		// DO NOT CHANGE!
		ds.setInitialSize(5);
		ds.setMaxTotal(10);
		System.out.println("ds created");
		return ds;
	}
	
	//the session factory bean
	@Bean
	public LocalSessionFactoryBean sessionFactory(DataSource dataSource) {
	  LocalSessionFactoryBean sfb = new LocalSessionFactoryBean();
	  sfb.setDataSource(dataSource);
	  //scan the packages looking for Hibernate annotations
	  sfb.setPackagesToScan(new String[] { "helloworld.model" });
	  Properties props = new Properties();
	  props.setProperty("dialect", "org.hibernate.dialect.MySQLDialect");
	  props.setProperty("hibernate.hbm2ddl.auto", "update");
	  props.setProperty("hibernate.show_sql", "true");
	  sfb.setHibernateProperties(props);
	  return sfb;
	}
	
	
	



}
