package helloworld.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

//also a configuration class
@Configuration
//tell where to look for beans - anywhere in the package, except where the enablewebmvc annotation is :-)
@ComponentScan(
		basePackages={"helloworld"},
		excludeFilters={
				@Filter(type=FilterType.ANNOTATION,value=EnableWebMvc.class)
		})
public class RootConfig {

}