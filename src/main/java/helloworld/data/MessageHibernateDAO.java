package helloworld.data;

import helloworld.model.Message;

import java.util.List;

import javax.inject.Inject;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class MessageHibernateDAO implements MessageRepository {
	
	private SessionFactory sessionFactory;
	
	@Inject
	public MessageHibernateDAO(SessionFactory sessionFactory) {
		System.out.println("Hibernate DAO created");
		this.sessionFactory = sessionFactory;
		
	}
	
	private Session newSession() {
		return sessionFactory.openSession();
	}
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<Message> getAll() {
		return (List<Message>) newSession().createCriteria(Message.class).addOrder(Order.desc("id")).list();
		
	}
	
	public void save(Message message) {
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			
			session.save(message);
			
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			System.err.println("Save failed!!!! " + e.getMessage());
		} finally {
			session.close();
		}
	}
	
	public Message get(long id) {
		return newSession().get(Message.class, id);
	}
	
	public void delete(Message m) {
		newSession().delete(m);
	}
	
	public List<Message> getMessagesBySender(String sender) {
		Session session = newSession();
		try {
			Query<Message> query = session.createQuery("from Message where sender = :sender", Message.class);
			query.setParameter("sender", sender);
			return query.getResultList();
		} catch (Exception e) {
			System.err.println("Listing " + sender + " failed!!!! " + e.getMessage());
			throw new RuntimeException(e);
		} finally {
			session.close();
		}
	}
	
	public void update(Message message) {
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			
			session.update(message);
			
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			System.err.println("Save failed!!!! " + e.getMessage());
		} finally {
			session.close();
		}
	}
	
	
}
