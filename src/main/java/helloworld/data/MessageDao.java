package helloworld.data;

import helloworld.model.Message;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;



public class MessageDao implements MessageRepository {
	private List<Message> messages = Collections.emptyList();
	
	public MessageDao() {
		messages = new ArrayList<Message>();
		addDummyMessages();
	}
	
	private void addDummyMessages() {
		save(new Message("P-J", "testing"));
		
		// Calculate yesterday
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_MONTH, -1);
		
		save(new Message("P-J", "testing2", c.getTime()));
		
	}
	
	public List<Message> getAll() {
		return messages;
	}
	
	public void save(Message m) {
		m.setId(messages.size());
		messages.add(m);
		
	}
	
	public Message get(long id) {
		Message result = null;
		if (id < messages.size()) {
			result = messages.get((int) id);
		}
		return result;
	}
	
	public void delete(Message m) {
		// TODO Auto-generated method stub
		
	}
	
	public List<Message> getMessagesBySender(String sender) {
		throw new UnsupportedOperationException("implement me, after test");
	}
	
	public void update(Message actualMessage) {
		throw new UnsupportedOperationException("implement me, after test");
	}
	
}
