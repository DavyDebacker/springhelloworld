package helloworld.data;

import helloworld.model.Message;

import java.util.List;

public interface MessageRepository {
	
	List<Message> getAll();
	
	public void save(Message m);
	
	public Message get(long id);
	
	public void delete(Message m);
	
	List<Message> getMessagesBySender(String sender);
	
	void update(Message actualMessage);
	
}
