package helloworld.data.test;

import java.util.Date;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import helloworld.config.DataConfig;
import helloworld.data.MessageRepository;
import helloworld.model.Message;


public class Main {

	public static void main(String[] args) {
		Configuration c =new Configuration()
				.addAnnotatedClass(Message.class)
				.setProperty("hibernate.connection.url", "jdbc:mysql://localhost:3306/pieterjd")
				.setProperty("hibernate.connection.username", "pieterjd")
				.setProperty("hibernate.connection.password", "19ABcd78")
				//next property creates tables if not yet existing
				.setProperty("hibernate.hbm2ddl.auto", "update")
				.setProperty("hibernate.show_sql", "true")
				;
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
		
		ctx.register(DataConfig.class);
		ctx.refresh();
		SessionFactory factory = ctx.getBean(SessionFactory.class);
		MessageRepository repo = ctx.getBean(MessageRepository.class);
		//Session s  =factory.openSession();
		
		
		
		Message m = repo.get(36);
		m.setMessage("updated on "+new Date());
		repo.save(m);
		/*
		Transaction t =s.beginTransaction();
		s.save(m);
		t.commit();
		*/
		//s.flush();
		//factory.getCurrentSession().flush();
		//System.exit(0);
	}

}
