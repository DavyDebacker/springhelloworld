package helloworld.web.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import helloworld.data.MessageRepository;
import helloworld.model.Message;

@RestController

@RequestMapping("/api/v1")
public class MessageRestController {
	
	@Autowired
	private MessageRepository messageRepository;
	
	
	@RequestMapping(value="messages",method=RequestMethod.GET)
	public List<Message> getMessages(){
		return messageRepository.getAll();
	}
	
	//The post is used to both create new messages as to update existing one
	@RequestMapping(value="messages",method=RequestMethod.POST)
	public @ResponseBody Message saveMessage(@RequestBody Message message){
		System.out.println("trying to save using rest");
		System.out.println(message.getMessage());
		messageRepository.save(message);
		System.out.println("saved method executed");
		return message;
	}
	
	
	@RequestMapping(value="messages/{id}",method=RequestMethod.GET)
	public Message getMessages(@PathVariable("id") long id){
		System.out.println("Rest get");
		return messageRepository.get(id);
	}
	
	
	@RequestMapping(value="messages/{id}", method=RequestMethod.POST)
	public Message updateMessage(@PathVariable("id") long id,@Valid Message m){
		System.out.println("Rest post specific message; id="+id);
		System.out.println("msg read?" + m.getRead());
		messageRepository.save(m);
		return m;
	}
	
}
