package helloworld.web;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import helloworld.data.MessageRepository;
import helloworld.model.Message;

@Controller
//@RequestMapping("/message")
public class MessageController {
	
	
	private MessageRepository messageRepository;
	@Autowired
	public MessageController(MessageRepository messageRepository) {
		this.messageRepository = messageRepository;
		System.out.println("message controller created. Repo info: "+messageRepository);
	}
	@RequestMapping(value="/messages",method=RequestMethod.GET)
	public String messages(Model m){
		//create dummy messages
		
		//System.out.println("Message controller- repo size: "+messageRepository.getAll().size());
		//add variables to the model, these will be available in the view
		m.addAttribute("messages", messageRepository.getAll());
		return "messages";
	}
	
	//show just 1 message
	@RequestMapping(value="/show/{messageId}",method=RequestMethod.GET)
	public String showOne(@PathVariable("messageId") int id,Model m){
		System.out.println("details for msg#"+id);
		Message msg = messageRepository.get(id);
		msg.setRead(true);
		//msg.setMessage(msg.getMessage()+ "updated.");
		messageRepository.save(msg);
		m.addAttribute("message", msg);
		return "message";
	}
	
	//show just 1 message for the popup (only content, no html tags nor title
		@RequestMapping(value="/show/popover/{messageId}",method=RequestMethod.GET)
		public String showOnePopoverContent(@PathVariable("messageId") int id,Model m){
			System.out.println("details for msg#"+id);
			Message msg = messageRepository.get(id);
			msg.setRead(true);
			//msg.setMessage(msg.getMessage()+ "updated.");
			messageRepository.save(msg);
			m.addAttribute("message", msg);
			return "popoverMessage";
		}
	
	//show form to add a message
	@RequestMapping(value="/addMessage",method=RequestMethod.GET)
	//whe using spring form tags, you need a fresh new message object in the model to bind to the form elements
	public String addMessage(Model m){
		Message newMsg = new Message();
		//get logged in user's name 
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		System.out.println("Name loggedin user:" +authentication.getName());
		newMsg.setSender(authentication.getName());
		m.addAttribute("message", newMsg);
		return "addMessageForm";
	}
	
	//process form submission to add a message
		@RequestMapping(value="/addMessage",method=RequestMethod.POST)
		public String addMessage(@Valid Message m,Errors errors){
			if(errors.hasErrors()){
				System.out.println("We have errors in the hole");
				return "addMessageForm";
			}
			System.out.println("About to save...");
			messageRepository.save(m);
			System.out.println("Saved.");
			//return "redirect:/show/"+m.getId();
			return "redirect:/messages";
		}
	
}
