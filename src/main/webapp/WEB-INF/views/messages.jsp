<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ page session="false"%>
<%@ page isELIgnored="false"%>
<html>
<head>
<title>Messages View</title>
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />">
</head>
<body>

	<div class="container">
		<div class="page-header">
			<h1>
				All Messages
			</h1>
		</div>
		<a href="addMessage">Add Message</a> <br />
		<table class="table">
			<thead>
				<tr>
					<th>Id</th>
					<th>Preview</th>
					<th>Date</th>
					<th>Read?</th>
					<th>Links</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${messages}" var="message">
					<tr>
						<td><c:out value="${message.id}" /></td>
						<td>
							<c:set var="preview" value="${fn:substring(message.message, 0, 5)}"/>
							<c:choose>
								<c:when test="${!message.read}">
									<strong><c:out value="${preview}" /></strong>
								</c:when>
								<c:otherwise>
									<c:out value="${preview}" />
								</c:otherwise>
							</c:choose>
							
						</td>
						<td><c:out value="${message.date}" /></td>
						<td><c:out value="${message.read}" /></td>
						<td><a href="<c:url value="show/${message.id}" />" target="_blank">Read Message</a></td>
					</tr>
					<!--  
		<li id="message_<c:out value="${message.id}"/>">
			<div class="messageMessage">
				<c:out value="${fn:substring(message.message, 0, 5)}" /> || <a href="show/${message.id}">READ MORE</a>
			</div>
			<div>
				<span class="messageDate"><c:out value="${message.date}" /></span>
				
			</div>
		</li>
		-->
				</c:forEach>
			</tbody>
		</table>
	</div>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js"/>"></script>

</body>
</html>
