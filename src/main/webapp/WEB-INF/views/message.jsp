<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ page session="false"%>
<%@ page isELIgnored="false"%>
<html>
<head>
<title>Message Detail</title>
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />">
</head>
</head>
<body>
	<div class="container">
		<h1>Message Details</h1>

		<br /> <a href=<s:url value="/messages" ></s:url>>All Messages</a> <br />
		From <strong>${message.sender}</strong>, sent on ${message.date} <br/> <br/>
		${message.message}
		<br />
	</div>
</body>
</html>
