<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- Add the spring form tags - makes it easier to bind input elements to model attributes -->
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
<%@ page session="false"%>
<%@ page isELIgnored="false"%>
<html>
<head>
<title>Add Message</title>
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />">
</head>
<body>


	<!-- 
	The name of each form element matches the properties/fields of the Message class.
	Note: there is no input element for the date.
	 -->
	<!-- commandName refers to an object of Message and binds its fields to the input elements - 
	 a bit like angulars 2way binding -->
	<div class="container">
		<h2>New Message</h2>
		<sf:form method="post" commandName="message">
			<!-- path refers to the field -->
			<div class="form-group">
				Name:
				<sf:input path="sender" cssClass="form-control" />
				<sf:errors path="sender" cssClass="alert-warning" />
				<br />
			</div>
			<div class="form-group">
				Message:
				<sf:textarea path="message" rows="4" cols="50"
					cssClass="form-control" />
				<sf:errors path="message" cssClass="alert-danger"/>
				<br />
			</div>
			<!-- submit button is not bind to the model-> makes sense :) -->
			<!-- old submit button, replace it by bootstrap button
		<input type="submit" value="Add" /> -->
			<button type="submit" class="btn btn-default">Add</button>
		</sf:form>
	</div>
	<!--  the old version
	<form method="post">
		Name: <input type="text" name="sender" /><br/>
      Message: <textarea rows="4" cols="50" name="message"></textarea><br/>
       <input type="submit" value="Add" />
	</form>  -->
</body>
</html>
