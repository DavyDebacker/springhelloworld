//depends on ngResource
//depends on ngResource
var app = angular.module('demo', ['ngResource']);

/*
app.factory('MsgService',function($resource){
	console.log('creating msgService');
	return $resource('http://localhost:8080/CrunchHelloWorld/api/v1/messages/:id',{id: "@id"});
});
*/
app.controller('MsgCtrl', function($scope, $http,$resource) {
	$scope.newMsg = {sender:"your name",message:"your message"};
	var MsgService = $resource('http://localhost:8080/CrunchHelloWorld/api/v1/messages/:id',{id: "@id"},{
	    //add my own new function. In this case, an update is a post to the general url without ids
	    update:{
	        url:'http://localhost:8080/CrunchHelloWorld/api/v1/messages',
	        method:'POST',
	        isArray:false
	    }
	});


	$scope.refresh = function(){
		$scope.messages = MsgService.query();
	};
	
	$scope.refresh();
	$scope.viewMessage = function(msg){
		
		$scope.currentMessage = msg;
		msg.read = true;
		MsgService.update(msg);
	};

	$scope.addMsg = function(){
		console.log("addmsg called!");
		console.log($scope.newMsg);
		var res = MsgService.save($scope.newMsg,function(){
			//REST returns saved object, add it to the messages list
			$scope.messages.push(res);
		});
		
		
	}
	$scope.save = function(){
		console.log($scope.newMsg);
		$http.post('http://localhost:8080/CrunchHelloWorld/api/v1/messages',$scope.newMsg);
		window.location.href = 'index.html';
	};


});